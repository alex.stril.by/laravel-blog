@extends('layouts.app')

@section('content')
    <form action="{{route('login-submit')}}" method="post">
        @csrf

        <p>
            <label>
                Email

                @error('email')
                <br><span>{{$message}}</span>
                @enderror

                <input type="email" name="email"
                       class="@error('email'){{'error'}}@enderror"
                       required>
            </label>
        </p>

        <p>
            <label>
                Password

                @error('password')
                <br><span>{{$message}}</span>
                @enderror

                <input type="password" name="password"
                       class="@error('password'){{'error'}}@enderror"
                       required>
            </label>
        </p>

        <button type="submit">Login</button>
    </form>

    <a href="{{route('register')}}">Register</a>
@endsection
