<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return back()->withErrors([
                'email' => 'Such user is noy found',
            ]);
        }

        if (\Auth::attempt($request->validated())) {
            $request->session()->regenerate();

            return redirect()->route('home');
        }

        return back()->withErrors([
            'password' => 'Password dont match',
        ]);
    }

    public function logout()
    {
        \Auth::logout();

        return redirect()->route('home');
    }
}
