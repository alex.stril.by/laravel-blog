<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Moschino | Minimalist Free HTML Portfolio by WowThemes.net</title>
    <link rel='stylesheet' href='{{asset('css/woocommerce-layout.css')}}' type='text/css' media='all'/>
    <link rel='stylesheet' href='{{asset('css/woocommerce-smallscreen.css')}}' type='text/css'
          media='only screen and (max-width: 768px)'/>
    <link rel='stylesheet' href='{{asset('css/woocommerce.css')}}' type='text/css' media='all'/>
    <link rel='stylesheet' href='{{asset('css/font-awesome.min.css')}}' type='text/css' media='all'/>
    <link rel='stylesheet' href='{{asset('css/style.css')}}' type='text/css' media='all'/>
    <link rel='stylesheet'
          href='https://fonts.googleapis.com/css?family=Oswald:400,500,700%7CRoboto:400,500,700%7CHerr+Von+Muellerhoff:400,500,700%7CQuattrocento+Sans:400,500,700'
          type='text/css' media='all'/>
    <link rel='stylesheet' href='{{asset('css/easy-responsive-shortcodes.css')}}' type='text/css' media='all'/>

    <script src='{{asset('js/jquery.js')}}'></script>
    <script src='{{asset('js/plugins.js')}}'></script>
    <script src='{{asset('js/scripts.js')}}'></script>
    <script src='{{asset('js/masonry.pkgd.min.js')}}'></script>

    <script src="{{mix('js/app.js')}}" defer></script>
</head>
<body class="home page page-template page-template-template-portfolio page-template-template-portfolio-php">
<div id="page">
    <div class="container">
    @include('regular.header')
    <!-- #masthead -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area column full">
                <main id="main" class="site-main">
                    @yield('content')
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- #content -->
    </div>
    <!-- .container -->
    @include('regular.footer')
    <a href="#top" class="smoothup" title="Back to top"><span class="genericon genericon-collapse"></span></a>
</div>
<!-- #page -->
</body>
</html>
